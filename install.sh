#!/usr/bin/env zsh
set -e

# This script will copy dotfiles to their appropriate location, as well as install a default software set using brew.
# It assumes that XCode command line tools have been installed, and that your SSH key has been copied to ~/.ssh

DEFAULT_PACKAGES="colordiff composer git imagemagick php vim wget"
DEFAULT_CASKS="atom beardedspice bitwarden discord firefox gimp iterm2 jetbrains-toolbox jqbx microsoft-edge sourcetree spotify teamviewer telegram-desktop thunderbird visual-studio-code"

echo -n "Installing Homebrew... "
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew analytics off > /dev/null 2>&1
brew tap beeftornado/rmtree > /dev/null 2>&1
echo "done"

echo -n "Creating directories in home dir... "
mkdir -p ~/Git/temp
echo "done"

echo -n "Installing Powerline fonts... "
git clone https://github.com/powerline/fonts.git --depth=1 --quiet --progress ~/Git/temp/fonts
cd ~/Git/temp/fonts
./install.sh > /dev/null 2>&1
echo "done"

echo -n "Installing oh-my-zsh... "
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" > /dev/null 2>&1
echo "done"

echo -n "Installing default software set... "
brew install "$DEFAULT_PACKAGES"
brew install --cask "$DEFAULT_CASKS"
echo "done"

echo -n "Configuring Git... "
git config --global user.name "Jonas Cl�ment"
git config --global push.default current
echo -n "Email? "
read EMAIL
git config --global user.email "$EMAIL"
echo " done"

echo -n "Copying dotfiles... "
HOME_DIR_FILES=".aliases .vimrc .zshrc"
for file in "$HOME_DIR_FILES"; do
	ln -s $(pwd)/"$file" ~/"$file"
done

echo "Installing iTerm2 shell integrations... "
curl -L https://iterm2.com/shell_integration/install_shell_integration_and_utilities.sh | bash
echo "done"

# Copying ssh config because changes shouldn't be written to the repo.
cp -f $(pwd)/ssh_config ~/.ssh/config
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub
ssh-add -K ~/.ssh/id_rsa
cp -iR oh-my-zsh/* ~/.oh-my-zsh
cd ~/.oh-my-zsh
git add . > /dev/null 2>&1
git commit -m "Setup-script oh-my-zsh changes" > /dev/null 2>&1
cd ~
source ~/.zshrc
echo "done"

echo -n "Cleaning up... "
rm -rf ~/Git/temp
echo "done" 
echo "Remember to set iTerm to use Meslo LG M DZ for Powerline regular 13px."
