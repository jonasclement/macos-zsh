syntax enable
color darkblue

" fix backspace button
set backspace=indent,eol,start
